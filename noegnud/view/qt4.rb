require 'Qt'

class View

  class Window
    @@window_counter = 0
    attr_reader :window_id
    def initialize controller
      @controller = controller
      @@window_counter += 1
      @window_id = @@window_counter
    end
    def Window.create type, controller
      case type
        when :message
          Message
        when :status
          Status
        when :map
          Map
        when :menu
          Menu
        when :text
          Text
        else
          Window
      end.new controller
    end
    def putstr attribute, message
      @controller.nhext_raw_print "[#{@window_id}:#{attribute}] #{message}"
    end
    def display blocking=false
      @controller.nhext_raw_print "unhandled `display' for #{self.class}"
    end
    def set_mapsize rows, cols, layers
    end
    def clear
      @controller.nhext_raw_print "unhandled `clear' for #{self.class}"
    end
    def curs x, y
      @controller.nhext_raw_print "unhandled `curs' for #{self.class}"
    end
    def print_glyph x, y, glyph
      @controller.nhext_raw_print "unhandled `print_glyph' for #{self.class}"
    end
  end

  class Message < Window
    MESSAGES_HISTORY = 20
    def initialize controller
      super
      clear

      @window = Qt::TextEdit.new
      @window.setLineWrapMode Qt::TextEdit::NoWrap
      @window.setReadOnly true

      @window.show
    end
    def putstr attribute, message
      @messages << {:attribute=>attribute,:message=>message}
      @messages.shift while @messages.length > MESSAGES_HISTORY
      nil
    end
    def clear
      @messages = []
    end
    def display blocking=false
      text = @messages.map {|item| item[:message] }.join("\n")
      @window.setPlainText text
      @window.repaint
    end
  end

  class Status < Window
    def initialize controller
      super
      clear
    end
    def putstr attribute, message
      @status << message
    end
    def clear
      @status = []
    end
    def display blocking=false
      @status.each { |message| puts "[STATUS] #{message}" }
    end
  end

  class Map < Window
    @@instance = nil
    def Map.instance
      @@instance
    end
    def initialize controller
      super
      @map = nil
      @@instance = self
      @cursor_x = 0
      @cursor_y = 0
    end
    def set_mapsize rows, cols, layers
      @rows = rows
      @cols = cols
      @layers = layers
    end
    def clear
      @map = Array.new( @rows, Array.new( @cols, Array.new( @layers ) ) )
    end
    def display blocking=false
      if @map
      else
        @controller.raw_print "no map to display"
      end
    end
    def cliparound x, y
      @clip_x = x
      @clip_y = y
    end
    def curs x, y
      @cursor_x = x
      @cursor_y = y
    end
    def print_glyph x, y, glyph
      @map[y][x][0] = glyph
    end
    def print_glyph_layered layers
      #p layers
    end
  end

  module CustomWidgets
    class TextDialog < Qt::Dialog
      def initialize text
        super()
        layout = Qt::VBoxLayout.new
        layout.setMargin 0
        layout.setSpacing 0

        textWidget = Qt::TextEdit.new
        textWidget.setLineWrapMode Qt::TextEdit::NoWrap
        textWidget.setReadOnly true
        textWidget.setPlainText text.strip

        ok = Qt::PushButton.new 'Ok'
        Qt::Object.connect( ok, SIGNAL('clicked()'), self, SLOT('close()') )

        layout.addWidget textWidget
        layout.addWidget ok
        setLayout layout

        show
      end
    end
  end

  class Text < Window
    def initialize controller
      super
      clear
    end
    def clear
      @text = []
    end
    def putstr attribute, message
      @text << {:attribute=>attribute,:message=>message}
    end
    def display blocking=false
      text = @text.map {|item| item[:message] }.join("\n")
      CustomWidgets::TextDialog.new( text ).exec
    end
  end

  class Menu < Text
    def initialize controller
      super
      @menuMode = false
    end
    def display blocking=false
      if @menuMode
      else
        super
      end
    end
  end

  def View.name
    "Qt4"
  end

  def View.raw_print message
    puts "[RAW] #{message}"
  end

  def raw_print message
    View.raw_print message
  end

  class Application < Qt::Application
    slots 'quit!()'
    def init
      @running = true
    end
    def running?
      @running
    end
    def run
      processEvents
    end
    def quit!
      @running = false
    end
  end

  def initialize
    @app = Application.new ARGV
  end

  def run
    connection = NhExt::NhExt.new(
      IO.popen( "~/slashem/local/bin/slashem", mode="r+" ),
      Controller )

    @app.init

    Signal.trap "INT" do
      View.raw_print "Local signal caught, aborting!"
      connection.controller.stop_listening!
      @app.quit!
    end

    while @app.running? do
      @app.run
    end

    connection.controller.stop_listening!
  end

end
