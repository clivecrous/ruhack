class View

  class Window
    @@window_counter = 0
    attr_reader :window_id
    def initialize controller
      @controller = controller
      @@window_counter += 1
      @window_id = @@window_counter
    end
    def Window.create type, controller
      case type
        when :message
          Message
        when :status
          Status
        when :map
          Map
        when :menu
          Menu
        when :text
          Text
        else
          Window
      end.new controller
    end
    def putstr attribute, message
      @controller.nhext_raw_print "[#{@window_id}:#{attribute}] #{message}"
    end
    def display blocking=false
      @controller.nhext_raw_print "unhandled `display' for #{self.class}"
    end
    def set_mapsize rows, cols, layers
    end
    def clear
      @controller.nhext_raw_print "unhandled `clear' for #{self.class}"
    end
    def curs x, y
      @controller.nhext_raw_print "unhandled `curs' for #{self.class}"
    end
    def print_glyph x, y, glyph
      @controller.nhext_raw_print "unhandled `print_glyph' for #{self.class}"
    end
  end

  class Message < Window
    MESSAGES_HISTORY = 20
    def initialize controller
      super
      clear
    end
    def putstr attribute, message
      @messages << message
      @messages.shift while @messages.length > MESSAGES_HISTORY
      nil
    end
    def clear
      @messages = []
    end
    def display blocking=false
      @messages.each { |message| puts "[MESSAGE] #{message}" }
    end
  end

  class Status < Window
    def initialize controller
      super
      clear
    end
    def putstr attribute, message
      @status << message
    end
    def clear
      @status = []
    end
    def display blocking=false
      @status.each { |message| puts "[STATUS] #{message}" }
    end
  end

  class Map < Window
    @@instance = nil
    def Map.instance
      @@instance
    end
    def initialize controller
      super
      @map = nil
      @@instance = self
      @cursor_x = 0
      @cursor_y = 0
    end
    def set_mapsize rows, cols, layers
      @rows = rows
      @cols = cols
      @layers = layers
    end
    def clear
      @map = Array.new( @rows, Array.new( @cols, Array.new( @layers ) ) )
    end
    def display blocking=false
      if @map
      else
        @controller.raw_print "no map to display"
      end
    end
    def cliparound x, y
      @clip_x = x
      @clip_y = y
    end
    def curs x, y
      @cursor_x = x
      @cursor_y = y
    end
    def print_glyph x, y, glyph
      @map[y][x][0] = glyph
    end
    def print_glyph_layered layers
      #p layers
    end
  end

  class Menu < Window
  end

  class Text < Window
  end

  def View.name
    "Debug"
  end

  def View.raw_print message
    puts "[RAW] #{message}"
  end

  def raw_print message
    View.raw_print message
  end

  def run
    connection = NhExt::NhExt.new(
      IO.popen( "~/slashem/local/bin/slashem", mode="r+" ),
      Controller )

    running = true

    Signal.trap "INT" do
      View.raw_print "Local signal caught, aborting!"
      connection.controller.stop_listening!
      running = false
    end

    while running do
      sleep 0.25
    end
  end

end
