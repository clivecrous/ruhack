class Controller < NhExt::SubProtocol_1
  def initialize io
    super
    @windows={}
    @screen=nil
    @quit = false
  end
  def nhext_create_nhwindow windowtype
    window = View::Window.create( windowtype, self )
    @windows[ window.window_id ] = window
    window.window_id.to_xdr
  end
  def nhext_exit_nhwindows message
    nhext_raw_print message if message.length > 0
    @quit = true
    nil
  end
  def nhext_init
    nil
  end
  def nhext_init_nhwindows parameters
    true.to_xdr + ARGV.to_xdr + ["color","hilite_pet"].to_xdr
  end
  def nhext_get_nh_event
    nil
  end
  def nhext_putstr window_id, attribute, message
    if @windows.has_key? window_id
      @windows[ window_id ].putstr attribute, message
    else
      nhext_raw_print message
    end
    nil
  end
  def nhext_cliparound x,y
    View::Map.instance.cliparound x,y
    nil
  end
  def nhext_clear_nhwindow window_id, rows, cols, layers
    if @windows.has_key? window_id
      window = @windows[ window_id ]
      window.set_mapsize rows, cols, layers
      window.clear 
    end
  end
  def nhext_curs window_id, x, y
    @windows[ window_id ].curs( x, y ) if @windows.has_key? window_id
    nil
  end
  def nhext_nhgetch
    STDIN.getc.to_xdr
  end
  def nhext_print_glyph window_id, x, y, glyph
    @windows[ window_id ].print_glyph( x, y, glyph ) if @windows.has_key? window_id
    nil
  end
  def nhext_print_glyph_layered window_id, layers
    @windows[ window_id ].print_glyph_layered( layers ) if @windows.has_key? window_id
    nil
  end
  def nhext_display_nhwindow window_id, blocking
    @windows[ window_id ].display( blocking ) if @windows.has_key?( window_id )
  end
  def nhext_raw_print message
    View.raw_print message
    nil
  end
  def nhext_player_selection role, race, gender, alignment
    role.to_xdr + race.to_xdr + gender.to_xdr + alignment.to_xdr + false.to_xdr
  end
end
