require 'model/nhext/xdr'

module NhExt

  class SubProtocol_1
    def SubProtocol_1.protocol
      1
    end

    def initialize io
      @io=io
      @procedures = [
        :init,
        :init_nhwindows,
        :player_selection,
        :askname,
        :get_nh_event,
        :exit_nhwindows,
        :suspend_nhwindows,
        :resume_nhwindows,
        :create_nhwindow,
        :clear_nhwindow,
        :display_nhwindow,
        :destroy_nhwindow,
        :curs,
        :putstr,
        :display_file,
        :start_menu,
        :add_menu,
        :end_menu,
        :select_menu,
        :message_menu,
        :update_inventory,
        :mark_sync,
        :wait_sync,
        :cliparound,
        :update_positionbar,
        :print_glyph,
        :raw_print,
        :raw_print_bold,
        :nhgetch,
        :nh_poskey,
        :nhbell,
        :doprev_message,
        :yn_function,
        :getlin,
        :get_ext_cmd,
        :number_pad,
        :delay_output,
        :change_color,
        :change_background,
        :set_font_name,
        :get_color_string,
        :start_screen,
        :end_screen,
        :outrip,
        :preference_update,
        :status,
        :print_glyph_layered,
        :send_config_file
      ]
      @listen_thread = nil
      @listening = false
    end

    def reply id, content
      if content.is_a? String
        message = ((id<<16)+(((content.length)+3)>>2)).to_xdr+content
        #p message.to_hex
        @io.write message
      else
        reply id, content.to_s
      end
    end

    def emptyreply
      reply 0, ""
    end

    def listen
      @listen_thread = Thread.new do
        @listening = true
        while @listening do
          binaryvalue = @io.read(4)
          break if binaryvalue == nil
          value = binaryvalue.nhext_pop_uint
          id = (value >> 16)
          length = (value & 0xffff) << 2
          if length > 0
            content = @io.read( length )
          else
            content = ""
          end
          if id-1 < @procedures.length
            procedure = @procedures[id-1]
            nhext_procedure = "nhext_#{procedure.to_s}"
            if self.methods.index nhext_procedure
              case procedure
                when :clear_nhwindow
                  reply 0,
                    send( nhext_procedure, content.nhext_pop_uint!,
                          content.nhext_pop_uint!, content.nhext_pop_uint!,
                          content.nhext_pop_uint! )
                when :cliparound
                  reply 0, send( nhext_procedure, content.nhext_pop_int!,
                                 content.nhext_pop_int! )
                when :create_nhwindow
                  reply 0,
                    send( nhext_procedure,
                          [:invalid,:message,:status,:map,:menu,:text][
                            content.nhext_pop_uint! ] )
                when :curs
                  reply 0,
                    send( nhext_procedure, content.nhext_pop_int!,
                          content.nhext_pop_int!, content.nhext_pop_int! ) 
                when :display_nhwindow
                  reply 0,
                    send( nhext_procedure, content.nhext_pop_uint!,
                          content.nhext_pop_bool! )
                when :exit_nhwindows
                  reply 0, send( nhext_procedure, content.nhext_pop_string! )
                when :get_nh_event
                  reply 0, send( nhext_procedure )
                when :init
                  reply 0, send( nhext_procedure )
                when :init_nhwindows
                  reply 0, send( nhext_procedure, content.nhext_pop_arguments! )
                when :nhgetch
                  reply 0, send( nhext_procedure )
                when :player_selection
                  reply 0,
                    send( nhext_procedure, content.nhext_pop_int!,
                          content.nhext_pop_int!, content.nhext_pop_int!,
                          content.nhext_pop_int! )
                when :print_glyph
                  reply 0,
                    send( nhext_procedure, content.nhext_pop_int!,
                          content.nhext_pop_int!, content.nhext_pop_int!,
                          content.nhext_pop_int! ) 
                when :print_glyph_layered
                  window_id = content.nhext_pop_uint!
                  layers = []
                  (0...content.nhext_pop_uint!).each do |layer_num|
                    layer={}
                    layer[:start] = content.nhext_pop_int!
                    layer[:rows] = []
                    (0...content.nhext_pop_uint!).each do
                      row={}
                      row[:start] = content.nhext_pop_int!
                      row[:glyphs] = []
                      (0...content.nhext_pop_uint!).each do
                        row[:glyphs] << content.nhext_pop_uint!
                      end
                      layer[:rows] << row
                    end
                    layers << layer
                  end
                  reply 0, send( nhext_procedure, window_id, layers )
                when :putstr
                  reply 0,
                    send( nhext_procedure, content.nhext_pop_uint!,
                                [:none,:bold,:dim,:unknown,:uline,:blink,
                                 :unknown,:inverse][ content.nhext_pop_uint! ],
                                content.nhext_pop_string! )
                when :raw_print
                  reply 0, send( nhext_procedure, content.nhext_pop_string! )
              else
                p "UNHANDLED: #{"0x%X"%id} #{procedure} : #{content}"
                reply 0, send(nhext_procedure)
              end
            else
              puts "[ERROR] your controller is missing: `#{nhext_procedure}'"
              emptyreply
            end
          else
            p "UNKNOWN: #{"0x%X"%id} : #{content}"
            emptyreply
          end
        end
      end
    end

    def listening?
      @listening
    end

    def stop_listening!
      @listening = false
      @listen_thread.join if @listen_thread
      @listen_thread = nil
    end

  end # class SubProtocol_1

  class NhExt
    attr_reader :header, :controller
    def initialize io, controller
      header = io.readline
      throw "Not a NHExt connection" if header[0,5] != "NhExt"
      @header = (header[6,header.length-6].strip+" ").split("\" ").inject({}) {|h,s| a=s.split(" \"");h[a[0].to_sym]=a[1];h}
      throw "No NhExt protocol information found" if !@header.has_key?(:protocols)
      @header[:protocols] = @header[:protocols].split(",").map{|n|n.to_i}
      if controller.instance_of? Array
        @controller = controller.inject( nil ) do |usecontroller,thiscontroller|
          if not usecontroller and @header[:protocols].index( thiscontroller.protocol )
            thiscontroller.new io
          else
            usecontroller
          end
        end
        throw "Requested protocol was not found" if !@controller
      else
        throw "Requested protocol was not found" if !@header[:protocols].index(controller.protocol)
        @controller = controller.new io
      end
      io.puts "Ack windowtype \"noegnud2::#{View::name}\" protocol \"#{@controller.class.protocol}\""
      @controller.listen
    end
  end # class NhExt

end # module NHExt
