class String
  def nhext_cut length
    throw "Protocol Error: length is #{self.length}, but #{length} was requested." if self.length < length
    self[0...length]
  end
  def nhext_cut! length
    cut=nhext_cut length
    replace self[length,self.length]
    cut
  end
  def nhext_pop_uint
    self.dup.nhext_pop_uint!
  end
  def nhext_pop_uint!
    uint = nhext_cut! 4
    (uint[0]<<24)+(uint[1]<<16)+(uint[2]<<8)+uint[3]
  end
  def nhext_pop_int
    self.dup.nhext_pop_int!
  end
  def nhext_pop_int!
    uint = nhext_pop_uint!
    uint >= 1<<31 ? uint-(1<<32) : uint
  end
  def nhext_pop_string
    dup.nhext_pop_string!
  end
  def nhext_pop_string!
    length=nhext_pop_uint!
    nhext_cut! length
  end
  def nhext_pop_arguments
    self.dup.nhext_pop_arguments!
  end
  def nhext_pop_arguments!
    arguments = []
    (1..nhext_pop_uint!).each { arguments << nhext_pop_string! }
    arguments
  end
  def nhext_pop_bool!
    nhext_pop_uint! == 1
  end
  def to_xdr
    "#{self.length.to_xdr}#{self}"+0.chr*((4-self.length%4)%4) 
  end
  def to_hex
    self.split('').inject('') {|result,char| result += "%02X"%char[0]}
  end
end

class Fixnum
  def to_xdr
    "#{((self >> 24) & 0xFF).chr}#{((self >> 16) & 0xFF).chr}#{((self >> 8) & 0xFF).chr}#{(self & 0xFF).chr}"
  end
end

class FalseClass
  def to_xdr
    0.to_xdr
  end
end

class TrueClass
  def to_xdr
    1.to_xdr
  end
end

class Array
  def to_xdr
    self.inject( self.length.to_xdr ) { |result,item| result += item.to_xdr }
  end
end
